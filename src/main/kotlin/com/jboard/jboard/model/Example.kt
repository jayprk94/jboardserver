package com.jboard.jboard.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Column

@Entity
class Example {
    @Id @GeneratedValue
    var id: Long? = null

    @Column(name="one")
    var one: Long? = null
}