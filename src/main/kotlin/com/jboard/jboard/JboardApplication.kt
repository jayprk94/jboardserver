package com.jboard.jboard

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JboardApplication

fun main(args: Array<String>) {
	runApplication<JboardApplication>(*args)
}
